# Projet Frontend vuejs

Le projet est en lien avec l'API : https://framagit.org/mathieugodard/la-coordonnerie-backend-api

Vous pouvez consulter le rappport du projet sur le lien [Notion](https://www.notion.so/Projet-de-la-Coordonnerie-8d665e0232904ebc8a63d9ca50340524)

## Build Setup

```bash
# go in the project
$ cd lacoordonnerie
```

```bash
# install dependencies
$ npm install
```
```bash
# serve with hot reload at localhost:3000
$ npm run dev
```
```bash
# build for production and launch server
$ npm run build
$ npm run start
```
```bash
# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
